defmodule Rumbl.Web.WatchController do
  use Rumbl.Web, :controller
  alias Rumbl.Data

  def show(conn, %{"id" => id}) do
    video = Data.get_video!(id)
    render conn, "show.html", video: video
  end
end
