defmodule Rumbl.Web.UserView do
  use Rumbl.Web, :view

  @doc """
  Helper view function that returns the first name of a User.
  """
  def first_name(%Rumbl.User{name: name}) do
    name
    |> String.split(" ")
    |> Enum.at(0)
  end

  def render("user.json", %{user: user}) do
    %{id: user.id, username: user.username}
  end
end
