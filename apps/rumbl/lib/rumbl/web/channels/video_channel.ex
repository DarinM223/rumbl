defmodule Rumbl.Web.VideoChannel do
  use Rumbl.Web, :channel

  import Ecto.Query

  alias Rumbl.Repo
  alias Rumbl.Data
  alias Rumbl.Web.AnnotationView

  def join("videos:" <> video_id, params, socket) do
    last_seen_id = params["last_seen_id"] || 0
    video_id = String.to_integer(video_id)
    video = Data.get_video!(video_id)

    annotations = Repo.all(
      from a in Ecto.assoc(video, :annotations),
        where: a.id > ^last_seen_id,
        order_by: [asc: a.at, asc: a.id],
        limit: 200,
        preload: [:user]
    )

    resp = %{annotations: Phoenix.View.render_many(annotations, AnnotationView,
                                                   "annotation.json")}

    {:ok, resp, assign(socket, :video_id, video_id)}
  end

  def handle_in(event, params, socket) do
    user = Repo.get(Rumbl.User, socket.assigns.user_id)
    handle_in(event, params, user, socket)
  end

  def handle_in("new_annotation", params, user, socket) do
    case Data.create_user_annotation(user, socket.assigns.video_id, params) do
      {:ok, annotation} ->
        broadcast_annotation(socket, annotation)
        Task.start_link(fn -> compute_additional_info(annotation, socket) end)
        {:reply, :ok, socket}
      {:error, changeset} ->
        {:reply, {:error, %{errors: changeset}}, socket}
    end
  end

  defp broadcast_annotation(socket, annotation) do
    annotation = Repo.preload(annotation, :user)
    rendered_ann = Phoenix.View.render(AnnotationView, "annotation.json", %{
      annotation: annotation
    })

    # Broadcast sends an event to all sockets in a topic.
    broadcast! socket, "new_annotation", rendered_ann
  end

  defp compute_additional_info(annotation, socket) do
    for result <- InfoSys.compute(annotation.body, limit: 1, timeout: 10_000) do
      attrs = %{url: result.url, body: result.text, at: annotation.at}
      case Data.create_info(result, annotation, attrs) do
        {:ok, annotation} -> broadcast_annotation(socket, annotation)
        {:error, _changeset} -> :ignore
      end
    end
  end
end
