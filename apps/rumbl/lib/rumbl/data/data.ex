defmodule Rumbl.Data do
  @moduledoc """
  The boundary for the Data system.
  """

  import Ecto.Query, warn: false
  alias Rumbl.Repo

  alias Rumbl.Data.Video
  alias Rumbl.Data.Category
  alias Rumbl.Data.Annotation

  @doc """
  Returns the list of videos.

  ## Examples

      iex> list_videos()
      [%Video{}, ...]

  """
  def list_videos do
    Repo.all(Video)
  end

  defp user_video(user) do
    Ecto.assoc(user, :videos)
  end

  @doc """
  Returns a list of videos that a user contains.
  """
  def list_user_videos(user) do
    Repo.all(user_video(user))
  end

  @doc """
  Gets a single video.

  Raises `Ecto.NoResultsError` if the Video does not exist.

  ## Examples

      iex> get_video!(123)
      %Video{}

      iex> get_video!(456)
      ** (Ecto.NoResultsError)

  """
  def get_video!(id), do: Repo.get!(Video, id)

  @doc """
  Gets a single user video.

  Raises `Ecto.NoResultsError` if the Video does not exist.
  """
  def get_user_video!(user, id) do
    Repo.get!(user_video(user), id)
  end

  @doc """
  Creates a video.

  ## Examples

      iex> create_video(%{field: value})
      {:ok, %Video{}}

      iex> create_video(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_video(attrs \\ %{}) do
    %Video{}
    |> Video.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Creates a user's video.
  """
  def create_user_video(user, attrs \\ %{}) do
    user
    |> Ecto.build_assoc(:videos)
    |> Video.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Creates a user's annotation.
  """
  def create_user_annotation(user, video_id, attrs \\ %{}) do
    user
    |> Ecto.build_assoc(:annotations, video_id: video_id)
    |> Annotation.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Creates an InfoSys annotation.
  """
  def create_info(%{backend: backend}, annotation, attrs \\ %{}) do
    Repo.get_by!(Rumbl.User, username: backend)
    |> Ecto.build_assoc(:annotations, video_id: annotation.video_id)
    |> Annotation.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a video.

  ## Examples

      iex> update_video(video, %{field: new_value})
      {:ok, %Video{}}

      iex> update_video(video, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_video(%Video{} = video, attrs) do
    video
    |> Video.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Video.

  ## Examples

      iex> delete_video(video)
      {:ok, %Video{}}

      iex> delete_video(video)
      {:error, %Ecto.Changeset{}}

  """
  def delete_video(%Video{} = video) do
    Repo.delete(video)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking video changes.

  ## Examples

      iex> change_video(video)
      %Ecto.Changeset{source: %Video{}}

  """
  def change_video(%Video{} = video) do
    Video.changeset(video, %{})
  end

  @doc """
  Returns a list of categories.
  """
  def list_categories do
    query =
      Category
      |> Category.alphabetical()
      |> Category.names_and_ids()
    Repo.all(query)
  end
end
