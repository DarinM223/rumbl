defmodule Rumbl.Data.Annotation do
  use Ecto.Schema
  import Ecto.Changeset
  alias Rumbl.Data.Annotation


  schema "annotations" do
    field :at, :integer
    field :body, :string
    belongs_to :user, Rumbl.User
    belongs_to :video, Rumbl.Data.Video

    timestamps()
  end

  @doc false
  def changeset(%Annotation{} = annotation, attrs) do
    annotation
    |> cast(attrs, [:body, :at])
    |> validate_required([:body, :at])
  end
end
