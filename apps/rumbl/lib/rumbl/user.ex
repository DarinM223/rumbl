defmodule Rumbl.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :name, :string
    field :username, :string
    field :password, :string, virtual: true # virtual fields are not persisted
    field :password_hash, :string
    has_many :videos, Rumbl.Data.Video
    has_many :annotations, Rumbl.Data.Annotation

    timestamps
  end

  @doc """
  Returns the changeset for a model with parameters.

  Used for updating non-password related fields.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, [:name, :username]) # Take fields name and username
    |> validate_required([:name, :username]) # Validate required fields
    |> validate_length(:username, min: 1, max: 20) # Validates length of username
    |> unique_constraint(:username)
  end

  @doc """
  Returns the changeset for a model with parameters.

  Used for updating password related fields.
  """
  def registration_changeset(model, params \\ :empty) do
    model
    |> changeset(params)
    |> cast(params, [:password])
    |> validate_required([:password])
    |> validate_length(:password, min: 6, max: 100)
    |> put_password_hash()
  end

  def put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}} ->
        put_change(changeset, :password_hash, Comeonin.Bcrypt.hashpwsalt(password))
      _ ->
        changeset
    end
  end
end
