defmodule Rumbl.Web.VideoViewTest do
  use Rumbl.Web.ConnCase, async: true
  import Phoenix.View

  alias Rumbl.Data
  alias Rumbl.Data.Video
  alias Rumbl.Web.VideoView

  test "renders index.html", %{conn: conn} do
    videos = [%Video{id: "1", title: "dogs"},
              %Video{id: "2", title: "cats"}]
    content = render_to_string(VideoView, "index.html", conn: conn, videos: videos)
    for video <- videos do
      assert String.contains?(content, video.title)
    end
  end

  test "renders new.html", %{conn: conn} do
    changeset = Data.change_video(%Video{})
    categories = [{"cats", 123}]
    content = render_to_string(VideoView, "new.html",conn: conn,
                               changeset: changeset, categories: categories)

    assert String.contains?(content, "New Video")
  end
end
