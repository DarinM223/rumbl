defmodule Rumbl.CategoryRepoTest do
  use Rumbl.DataCase

  alias Rumbl.Data.Category

  test "alphabetical/1 orders by name" do
    Repo.insert!(%Category{name: "c"})
    Repo.insert!(%Category{name: "a"})
    Repo.insert!(%Category{name: "b"})

    query = Category.alphabetical(Category)
    query = from c in query, select: c.name
    assert Repo.all(query) == ~w(a b c)
  end
end
