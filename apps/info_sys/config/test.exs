use Mix.Config

config :logger,
  backends: [:console],
  compile_time_purge_level: :debug

config :info_sys, :wolfram,
  app_id: "1234",
  http_client: InfoSys.Test.HttpClient
